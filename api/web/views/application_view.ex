defmodule LiveChat.ApplicationView do
  use LiveChat.Web, :view

  def render("not_found.json", _) do
    %{error: "Not found"}
  end
end
