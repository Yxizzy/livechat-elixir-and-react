defmodule LiveChat.VisitorView do
  use LiveChat.Web, :view

  def render("index.json", %{visitors: visitors}) do
    %{data: render_many(visitors, LiveChat.VisitorView, "visitor.json")}
  end

  def render("show.json", %{visitor: visitor}) do
    %{data: render_one(visitor, LiveChat.VisitorView, "visitor.json")}
  end

  def render("visitor.json", %{visitor: visitor}) do
    %{id: visitor.id,
      email: visitor.email}
  end
end
