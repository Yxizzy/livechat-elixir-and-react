defmodule LiveChat.VisitorController do
  use LiveChat.Web, :controller

  alias LiveChat.Visitor

  plug Guardian.Plug.EnsureAuthenticated, [handler: LiveChat.SessionController] when action in [:rooms]

  def create(conn, params) do
    changeset = Visitor.changeset(%Visitor{}, params)

    case Repo.insert(changeset) do
      {:ok, visitor} ->
        new_conn = Guardian.Plug.api_sign_in(conn, visitor, :access)
        jwt = Guardian.Plug.current_token(new_conn)

        new_conn
        |> put_status(:created)
        |> render(LiveChat.SessionView, "start.json", visitor: visitor, jwt: jwt)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(LiveChat.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def rooms(conn, params) do
    current_user = Guardian.Plug.current_resource(conn)
    page =
      assoc(current_user, :rooms)
      |> Repo.paginate(params)
    render(conn, LiveChat.RoomView, "index.json", page: page)
  end
end
