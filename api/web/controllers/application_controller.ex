defmodule LiveChat.ApplicationController do
  use LiveChat.Web, :controller

  def not_found(conn, _params) do
    conn
    |> put_status(:not_found)
    |> render(LiveChat.ApplicationView, "not_found.json")
  end
end
