defmodule LiveChat.Message do
  use LiveChat.Web, :model

  schema "messages" do
    field :text, :string
    belongs_to :room, LiveChat.Room
    belongs_to :user, LiveChat.User

    timestamps()
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:text, :user_id, :room_id])
    |> validate_required([:text, :user_id, :room_id])
  end
end
