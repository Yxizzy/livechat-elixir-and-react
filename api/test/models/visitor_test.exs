defmodule LiveChat.VisitorTest do
  use LiveChat.ModelCase

  alias LiveChat.Visitor

  @valid_attrs %{email: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Visitor.changeset(%Visitor{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Visitor.changeset(%Visitor{}, @invalid_attrs)
    refute changeset.valid?
  end
end
